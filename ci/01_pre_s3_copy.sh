#!/bin/bash

if [ ! -z $BITBUCKET_PR_ID ]
then
    export BUILD_BUCKET_DIR="${DYNAMIC_BUCKET}/${BITBUCKET_PR_ID}"
    
else
    export BUILD_BUCKET_DIR="${DYNAMIC_BUCKET}/${BITBUCKET_BRANCH}"
fi


if  [ "$BITBUCKET_BRANCH" = "master" ]
then
    echo "Publishing new code to: production ...."
    export BUILD_BUCKET_DIR="${MASTER_BUCKET}"
    echo "Sync files to:"
    echo $BUILD_BUCKET_DIR
    echo $AWS_ACCESS_KEY_ID
fi

# echo "Publishing new code to:"
# echo aws iam list-account-aliases | jq '.AccountAliases[0]'

echo "Sync files to:"
echo $BUILD_BUCKET_DIR
