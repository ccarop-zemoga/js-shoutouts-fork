#!/bin/bash
# BITBUCKET_PR_ID="58"
# BITBUCKET_BRANCH=test


if [ ! -z $BITBUCKET_PR_ID ]
then
    domain_prefix=${BITBUCKET_PR_ID}    
else
    domain_prefix=$BITBUCKET_BRANCH
fi

hosted_zone=Z08632952FOMQEQCH62TP
DNSName=js-shouts.zemoga.dev.
domain_name=${domain_prefix}.${DNSName}

echo "Attemping to Create the following dynamic URL:"
echo ${domain_name}
# Creates route 53 records based on env name

aws route53 change-resource-record-sets --hosted-zone-id ${hosted_zone}  --change-batch '
{
    "Comment": "Creating Alias resource record sets in Route 53",
    "Changes": [{
               "Action": "CREATE",
               "ResourceRecordSet": {
                           "Name": "'${domain_name}'",
                           "Type": "A",
                           "AliasTarget":{
                                   "HostedZoneId": "'${hosted_zone}'",
                                   "DNSName": "'${DNSName}'",
                                   "EvaluateTargetHealth": false
                             }}
                         }]
}
'