#!/bin/bash

echo $BITBUCKET_PR_ID
echo $BITBUCKET_BRANCH
echo $BUILD_BUCKET_DIR
echo $DISTRIBUTION_ID

# CMSable branches
# Bash Array values in "" and separated by space.
cms_branches=("staging-preview" "preview" "qa-preview" "develop")
if [[ ${cms_branches[*]} =~ (^|[[:space:]])$BITBUCKET_BRANCH($|[[:space:]]) ]]
then
    echo "Exporting env vars for cms, NETLIFY_APP_ID and GATSBY_CMS_DEPLOYMENT_BRANCH"
    BRANCH=$(echo $BITBUCKET_BRANCH | awk '{ print toupper($0) }')
    export GATSBY_CMS_DEPLOYMENT_BRANCH=$BITBUCKET_BRANCH
    env_var_name=NETLIFY_APP_ID_${BRANCH/-/_}
    export NETLIFY_APP_ID=${!env_var_name}
else
    echo "Not building CMS since its not part of the scope for this branch"
fi
