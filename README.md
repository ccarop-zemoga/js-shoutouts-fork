<p align="center">
    <img alt="Js-shouts" src="./src/images/Logo-2.png" width="100" height="250" />
</p>

<h3 align="center">
  Satisfying a CI/CD process in a team with rapidly code production in JAM stack

</h3>

This repository has only educational purposes, the use of it is purely demontrational and no contribution guidlines are needed.

<h3>Additional resources</h3>
<a href="https://docs.google.com/presentation/d/1i68MRDBLvEs1JwzL8ytKYHL_GSDUgHU0gyzCggdB6VQ/edit?usp=sharing">
    Complete presentation - google slides
  </a>


## 🚀 Quick start

1.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```shell
    npm run develop
    ```

2.  **Open the code and start customizing!**

    Your site is now running at http://localhost:8000!

    Edit `src/pages/index.js` to see your site update in real-time!

4.  **Learn more**

    - [Documentation](https://www.gatsbyjs.com/docs/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Tutorials](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Guides](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [API Reference](https://www.gatsbyjs.com/docs/api-reference/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Plugin Library](https://www.gatsbyjs.com/plugins?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Cheat Sheet](https://www.gatsbyjs.com/docs/cheat-sheet/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)
